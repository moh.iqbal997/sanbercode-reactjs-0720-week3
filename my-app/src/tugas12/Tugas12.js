import React, {Component} from 'react'



// Clock
function FormattedDate(props) {
    return <h1>sekarang jam: {props.date.toLocaleTimeString()}.</h1>;
  }
  
  class Clock extends React.Component {
    constructor(props) {
      super(props);
      this.state = {date: new Date()};
    }
  
    componentDidMount() {
      this.timerID = setInterval(
        () => this.tick(),
        1000
      );
    }
  
    componentWillUnmount() {
      clearInterval(this.timerID);
    }
  
    tick() {
      this.setState({
        date: new Date()
      });
    }

    render() {
        return (
            <FormattedDate date={this.state.date} />
        );
      }
    }
  









// Timer

class Timer extends Component{
  constructor(props){
    super(props)
    this.state = {
      time: 100
    }

  }

  componentDidMount(){
    if (this.props.start !== undefined){
      this.setState({time: this.props.start})
    }
    this.timerID = setInterval(
      () => this.tick(),
      1000
    );
  }

  componentWillUnmount(){
    clearInterval(this.timerID);
  }

  tick() {
    this.setState({
      time: this.state.time - 1 
    });
  }



render(){
    return(
      <>
      {this.state.time >= 0 &&
        <div className="box-tugas12">
            <Clock />
            <h1>
            hitung mundur: {this.state.time}
        </h1>
        </div>

      }
                
      </>
    )
  }
}


export default Timer