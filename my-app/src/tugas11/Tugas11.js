import React from 'react';

class Tabel extends React.Component {
    render() {
        return (
            <>
            <h1 className='title'>Tabel Harga Buah</h1>
            <table>
                <thead>
                    <tr>
                        <th className="colm-nama">Nama</th>
                        <th className="colm-harga">Harga</th>
                        <th className="colm-berat">Berat</th>
                    </tr>
                </thead>
                <tbody>
                {dataHargaBuah.map(el => {
                return (
                    <tr>
                        <NamaBuah nama={el.nama}/>
                        <HargaBuah harga={el.harga}/>
                        <BeratBuah berat={el.berat / 1000}/>                    
                    </tr>
                )
            })}
                </tbody>
            </table>

           </>
        )
    }
}

let dataHargaBuah = [
    {nama: "Semangka", harga: 10000, berat: 1000},
    {nama: "Anggur", harga: 40000, berat: 500},
    {nama: "Strawberry", harga: 30000, berat: 400},
    {nama: "Jeruk", harga: 30000, berat: 1000},
    {nama: "Mangga", harga: 30000, berat: 500}
  ]

class NamaBuah extends React.Component {
    render() {
        return <td>{this.props.nama}</td>
    }
}

class HargaBuah extends React.Component {
    render() {
    return <td>{this.props.harga}</td>
    }
}

class BeratBuah extends React.Component {
    render() {
    return <td>{this.props.berat} Kg</td>
    }
}


export default Tabel