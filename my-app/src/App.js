import React from 'react';
import logo from './logo.svg';
import './App.css';
import Tugas11 from './tugas11/Tugas11';
import Tugas12 from './tugas12/Tugas12'

function App() {
  return (

    <>
      {/* Tugas Hari Ke-11 */}
      <Tugas11 />

      {/* Tugas Hari Ke-12 */}
      <Tugas12 />
    </>
  );
}

export default App;


